package com.keepdata.filencoder.service;

import com.keepdata.filencoder.component.FileCoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;

@Service
public class FileEncoderService {
    @Value("${source.file.path}")
    private String sourceFileName;
    @Value("${encoded.file.path}")
    private String encodedFileName;
    @Value("${decoded.file.path}")
    private String decodedFileName;

    /**
     * Шифрование файла
     *
     * @return возвращает сообщение выполнения операции шифрования файла
     * @throws FileNotFoundException в случае отсутствия файла для чтения
     */
    public String encodeBytes() throws FileNotFoundException {
        FileCoder fileCoder = new FileCoder(sourceFileName, encodedFileName);
        while (fileCoder.getSourceFileAvailability() > 0){
            byte[] buffer = fileCoder.getEncodeKey();
            fileCoder.writeEncodedKey(buffer);
        }
        fileCoder.closeResource();
        fileCoder.closeTarget();
        return "File was successfully encrypted";
    }

    public String decodeBytes() throws FileNotFoundException {
        FileCoder fileCoder = new FileCoder(encodedFileName, decodedFileName);
        while (fileCoder.getSourceFileAvailability() > 0) {
            byte[] buffer = fileCoder.getDecodeKey();
            fileCoder.writeEncodedKey(buffer);
        }
        fileCoder.closeResource();
        fileCoder.closeTarget();
        return "File was successfully decrypted";
    }
}
