package com.keepdata.filencoder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilencoderApplication {
    public static void main(String[] args) {
        SpringApplication.run(FilencoderApplication.class, args);
    }
}
