package com.keepdata.filencoder.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class FileCoder {
    private static final Logger logger = LoggerFactory.getLogger(FileCoder.class);

    private FileInputStream reader;
    private FileOutputStream writer;
    private File source;
    private File target;
    private int lastChunkOfBytes;

    public FileCoder(String sourceFile, String targetFile) throws FileNotFoundException {

        this.source = checkResourceFile(sourceFile);
        this.target = new File(targetFile);
        this.reader = initResource();
        this.writer = initTarget();
    }

    /**
     * Проверка наличия файла источника по указанному пути
     *
     * @param filePath - полный путь файла источника
     * @return возвращает файл по указанному пути
     * @throws FileNotFoundException в случае отсутствия файла
     */
    @NotNull
    private File checkResourceFile(@NotBlank String filePath) throws FileNotFoundException {
        Path path = Paths.get(filePath);
        if (!Files.exists(path)) {
            throw new FileNotFoundException("Source file not found");
        }
        return new File(filePath);
    }

    /**
     * Открытие потока для чтения файла
     *
     * @return возвращает поток для чтения файла
     * @throws FileNotFoundException в случае отсутствия файла
     */
    @NotNull
    private FileInputStream initResource() throws FileNotFoundException {
        return new FileInputStream(source);
    }

    /**
     * Открытие потока для записи в файл
     *
     * @return возвращает поток для записи в файл
     * @throws FileNotFoundException в случае отсутствия файла
     */
    @NotNull
    private FileOutputStream initTarget() throws FileNotFoundException {
        return new FileOutputStream(target);
    }

    /**
     * Закрытие потоко чтения файла
     */
    public void closeResource() {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }

    /**
     * Закрытие потока записи в файл
     */
    public void closeTarget() {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }

    /**
     * Получение закодированного массива байт из файла
     *
     * @return закодированный массив считанных байтов из файла
     */
    public byte[] getEncodeKey() {
        logger.info("Source file size: {} byte", getSourceFileAvailability());
        return encodeBuffer(readBuffer());
    }

    /**
     * Шифрование массива байт
     *
     * @param buffer - массив байтов для кодирования
     * @return возвращает закодированный массив байтов
     */
    public byte[] encodeBuffer(byte[] buffer) {
        byte[] toEncode = new byte[10];
        for (int i = 0; i < 10; i++) {
            switch (i) {
                case 0:
                    toEncode[3] = buffer[i];
                    break;
                case 1:
                    toEncode[5] = buffer[i];
                    break;
                case 2:
                    toEncode[9] = buffer[i];
                    break;
                case 3:
                    toEncode[1] = buffer[i];
                    break;
                case 4:
                    toEncode[6] = buffer[i];
                    break;
                case 5:
                    toEncode[4] = buffer[i];
                    break;
                case 6:
                    toEncode[7] = buffer[i];
                    break;
                case 7:
                    toEncode[2] = buffer[i];
                    break;
                case 8:
                    toEncode[0] = buffer[i];
                    break;
                case 9:
                    toEncode[8] = buffer[i];
                    break;
                default:
                    toEncode[i] = '0';
            }
        }
        logger.info("Encryption result {}", Arrays.toString(toEncode));
        return toEncode;
    }

    /**
     * Получение размера файла в байтах
     *
     * @return возвращает размер файла в байтах
     */
    public int getSourceFileAvailability() {
        try {
            return reader.available();
        } catch (IOException e) {
            logger.error(e.getMessage());
            return 0;
        }
    }

    /**
     * Побайтовое чтение файла
     *
     * @return возвращает массив считанных байтов
     */
    public byte[] readBuffer() {
        try {
            byte[] buffer = new byte[]{32, 32, 32, 32, 32, 32, 32, 32, 32, 32};
            int c;
            for (int i = 0; i < 10; i++) {
                if ((c = reader.read()) == -1){
                    break;
                }
                buffer[i] = (byte) c;
            }
            logger.info("Read bytes: {}", buffer);
            return buffer;
        } catch (IOException e) {
            logger.error(e.getMessage());
            return new byte[10];
        }
    }

    /**
     * Получение разкодированного массива байт из файла
     *
     * @return разкодированный массив считанных байтов из файла
     */
    public byte[] getDecodeKey() {
        logger.info("Source file size: {} byte", getSourceFileAvailability());
        return decodeBuffer(readBuffer());
    }

    /**
     * Дешифрование массива байт
     *
     * @param buffer - массив байтов для декодирования
     * @return возвращает декодированный массив байтов
     */
    public byte[] decodeBuffer(byte[] buffer){
        byte[] toDecode = new byte[10];
        for (int i = 0; i < 10; i++){
            switch (i) {
                case 0:
                    toDecode[8] = buffer[i];
                    break;
                case 1:
                    toDecode[3] = buffer[i];
                    break;
                case 2:
                    toDecode[7] = buffer[i];
                    break;
                case 3:
                    toDecode[0] = buffer[i];
                    break;
                case 4:
                    toDecode[5] = buffer[i];
                    break;
                case 5:
                    toDecode[1] = buffer[i];
                    break;
                case 6:
                    toDecode[4] = buffer[i];
                    break;
                case 7:
                    toDecode[6] = buffer[i];
                    break;
                case 8:
                    toDecode[9] = buffer[i];
                    break;
                case 9:
                    toDecode[2] = buffer[i];
                    break;
                default:
                    toDecode[i] = '0';
            }
        }
        logger.info("Decryption result {}", Arrays.toString(toDecode));
        return toDecode;
    }

    /**
     * Запись массива байт в файл
     *
     * @param buffer - массив байтов
     */
    public void writeEncodedKey(byte[] buffer) {
        try {
            writer.write(buffer);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}
