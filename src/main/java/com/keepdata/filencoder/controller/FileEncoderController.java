package com.keepdata.filencoder.controller;

import com.keepdata.filencoder.service.FileEncoderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;

@RestController
@RequestMapping("/api/v1/fileencoder")
public class FileEncoderController {
    private final FileEncoderService fileEncoderService;

    public FileEncoderController(FileEncoderService fileEncoderService) {
        this.fileEncoderService = fileEncoderService;
    }

    @GetMapping
    public String startFileProcessing() throws FileNotFoundException {
        return fileEncoderService.encodeBytes();
    }

    @GetMapping("/decode")
    public String startFileDecodeProcessing() throws FileNotFoundException {
        return fileEncoderService.decodeBytes();
    }
}
